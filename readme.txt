Sensorimotor circuits repository.

Author: Nathan GREINER, PhD candidate
Institution: EPFL BMI SV UPCOURTINE
email: nathan.greiner@epfl.ch


Contains sensorimotor circuit datasets exported from the Matlab-Comsol package FEM_EES_SC, and the results of simulations performed on these datasets using the Python-NEURON package SMCEES.